
/*
 * Given a string s and an array of string words, determine whether s is a prefix string of words.
A string s is a prefix string of words if s can be made by concatenating the first k strings in words
for some positive k no larger than words.length.
Return true if s is a prefix string of words, or false otherwise.
Example 1:
Input: s = "iloveleetcode", words = ["i","love","leetcode","apples"]
Output: true
Explanation:
s can be made by concatenating "i", "love", and "leetcode" together.
Example 2:
Input: s = "iloveleetcode", words = ["apples","i","love","leetcode"]
Output: false
Explanation:
It is impossible to make s using a prefix of arr.

Constraints:
1 <= words.length <= 100
1 <= words[i].length <= 20
1 <= s.length <= 1000
words[i] and s consist of only lowercase English lettter.
*/

import java.util.*;
class PrefixString{

	static boolean preString(String[] arr,String s1){
		String temp="";
		for(int i=0;i<arr.length;i++){
			temp=temp.concat(arr[i]);
			if(temp.equals(s1)){
				return true;
			}
		}
		return false;
	}

 	public static void main(String []args){

                Scanner sc=new Scanner(System.in);
                System.out.println("Enter The Size Of Array:");
                int N =sc.nextInt();
                String arr[]=new String[N];

                System.out.println("Enter The Array Elements:");
                for(int i=0;i<N;i++){
                        arr[i]=sc.next();
                }
                System.out.println("Enter The String:");
		String s1=sc.next();
		 
		System.out.println("RESULT:"+preString(arr,s1));
	}
}
