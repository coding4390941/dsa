
/*
 * Given an array of non-negative integers representing a number, implement a function to
simulate the carry forward operation that occurs when adding 1 to the number represented by
the array. The array represents the digits of the number, where the 0th index is the least
significant digit. Your task is to handle the carry forward operation correctly, updating the array
accordingly. The function should return the resulting array.
For example, given the input array [1, 9, 9], representing the number 199, the function should
return [2, 0, 0], representing the result of adding 1 to 199 with the carry forward properly
handled.
Consider edge cases such as when the number has trailing zeros or when the carry forward
results in an additional digit. Optimize your solution for efficiency and discuss the time and
space complexity of your algorithm.
*/

import java.util.*;

class CarryForward{

	static int[] carryForward(int []arr){
		int count=0;
		for(int i=1;i<arr.length;i++){
			if(arr[1]==9){
				arr[0]=1;
			}
			if(arr[i]!=9){
				arr[i]=arr[i]+1;
			}else {
				arr[i]=0;
			}
		}
		
		return arr;
	}
        public static void main(String []args){

                Scanner sc=new Scanner(System.in);
                System.out.println("Enter The Size Of Array:");
                int N =sc.nextInt();
                int arr[]=new int[++N];

                System.out.println("Enter The Array Elements:");
	
		//arr[0]=7;
                for(int i=1;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
		arr=carryForward(arr);
		
		for(int i=0;i<arr.length;i++){
                	if(i==0 && arr[0]!=1){
				continue;
			}
			System.out.println(arr[i]);
		}
	}
}
