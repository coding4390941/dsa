
/*Given an array of positive integers nums and a positive integer target, return the minimal length
of a
subarray
whose sum is greater than or equal to target. If there is no such subarray, return 0 instead.

Example 1:
Input: target = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: The subarray [4,3] has the minimal length under the problem constraint.
Example 2:
Input: target = 4, nums = [1,4,4]
Output: 1
Example 3:
Input: target = 11, nums = [1,1,1,1,1,1,1,1]
Output: 0

Constraints:
1 <= target <= 109
1 <= nums.length <= 105
1 <= nums[i] <= 104
*/

import java.util.*;
class MinLenOfSubArray{

	public static void main(String []args){

                Scanner sc=new Scanner(System.in);
                System.out.println("Enter The Size Of Array:");
                int N =sc.nextInt();
                int arr[]=new int[N];

                System.out.println("Enter The Array Elements:");

                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                
		System.out.println("Enter The Target:");
		int k=sc.nextInt();
		int res=findminLen(arr,k);
		if(res!=0)
                System.out.println("minlength:"+res);

	}
	int findminLen(int arr[],int k){

		int minLen=arr.length;
		int N=arr.length;
		for(int i=0;i<N;i++){
			int count=0;
			int sum=0;
			for(int j=i;j<N;j++){
				sum=sum+arr[j];
				count++;
			if(sum>=k){
				if(sum==k){

				if(count<minLen){
					minLen=count;
				
				}
			
			
				}

			}
		
			}
		
		}
		
		if(minLen==arr.length){
			return 0;
		}

		return minLen;
	}
}
