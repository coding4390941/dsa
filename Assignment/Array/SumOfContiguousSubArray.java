/*
 *	 Given an array containing n integers. The problem is to find the sum of the
	elements of the contiguous subarray having the smallest(minimum) sum.
	Examples:
	Input : arr[] = {3, -4, 2, -3, -1, 7, -5}
	Output : -6
	Subarray is {-4, 2, -3, -1} = -6
	Input : arr = {2, 6, 8, 1, 4}
	Output : 1
	
						*/

/// BRUTE FORCE APPROACH
/*
import java.util.*;

class SumOfSubArray{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int N=sc.nextInt();
		int arr[]=new int[N];

		System.out.println("Enter The Array Elements:");
		for(int i=0;i<N;i++){

			arr[i]=sc.nextInt();
		}
		int max=Integer.MIN_VALUE;
		int min=Integer.MAX_VALUE;
		for(int i=0;i<N;i++){

			if(arr[i]>max){
				max=arr[i];
			}
			if(arr[i]<min){
				min=arr[i];
			}
		}
			
	int sum=Integer.MAX_VALUE;
	for(int i=0;i<N;i++){

	int sum1=0;	
		for(int j=0;j<N;j++){
			int sum2=0;
			for(int k=i;k<=j;k++){
				sum2+=arr[k];
			}
			if(sum2<sum1){

				sum1=sum2;
			}
		}
		if(sum1<sum){
			sum=sum1;
		}
	}
	if(sum==0){

		System.out.println("MINIMUM SUM: "+min);
	}else{
	System.out.println("MINIMUM SUBARAY SUM: "+sum);

	}


	}
}
*/
///     OPTIMIZED WAY
import java.util.*;import java.util.*;

class SumOfSubArray{

        public static void main(String []args){

                Scanner sc=new Scanner(System.in);
                System.out.println("Enter The Size Of Array:");
                int N =sc.nextInt();
                int arr[]=new int[N];

                System.out.println("Enter The Array Elements:");
                for(int i=0;i<N;i++){
                        arr[i]=sc.nextInt();
                
		}
		int s1=Integer.MAX_VALUE;
		int s2=Integer.MAX_VALUE;
		int sum1=0;
		int sum2=0;
		for(int i=minIndex;i<N;i++){
			sum1+=arr[i];
			if(s1>sum1){
				s1=sum1;
			}
		}
		for(int i=maxIndex;i<N;i++){
			sum2+=arr[i];
			if(s1>sum2){
				s1=sum2;
			}
		}	
			System.out.println("Sum:"+s1);
		
	}
}



















