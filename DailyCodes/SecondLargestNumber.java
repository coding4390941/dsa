
//  SECOND LARGEST NUMBER

import java.util.*;
class SecondLargestNo{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Size Of Array:");
		int N=sc.nextInt();
		int arr[]=new int[N];
		System.out.println("Enter The Array Elemets:");
		for(int i=0;i<N;i++){

			arr[i]=sc.nextInt();
		}

		int max=Integer.MIN_VALUE;
		int max2=Integer.MIN_VALUE;
		for(int i=0;i<N;i++){

			if(arr[i]>max){
				max2=max;
				max=arr[i];
			}else if(arr[i]>max2 && arr[i]!=max){
				max2=arr[i];
			}
		}

		System.out.println("Second largest Number is :"+max2);
	}
}
