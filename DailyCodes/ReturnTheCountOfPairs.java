
//    RETURN THE COUNT OF PAIRS: WHOSE SUM IS 'K'  i.e. arr[i]+arr[[j]=k


class ReturnCountOfPairs{

	public static void main(String []args){

		int N=10;
		int k=10;
		int count=0;

		int arr[]=new int[]{3,5,2,1,-3,7,8,15,6,13};

		for(int i=0;i<N;i++){
			for(int j=i+1;j<N;j++){

				if(i!=j && arr[i]+arr[j]==k){

					count++;
				}
			}
		}

		System.out.println("Enter The Count :"+count);
	}
}	
