 //
//              PREFIX SUM
//              Given array of size N & Q no. of queries  queries containing two para. (s,e)
//              for all queries print summ of all elements from s to e.

import java.util.*;
class PrefixSum{

        static void totalsum(int arr[],int s,int e){

                int sum=0;
                for(int i=s;i<=e;i++){

                        sum=sum+arr[i];
                }
                System.out.println("Sum= "+sum);

        }

        public static void main(String []args){

                Scanner sc=new Scanner(System.in);
                System.out.println("Enter Size Of Array:");
                int N=sc.nextInt();
                int arr[]=new int[N];
                System.out.println("Enter The Array Elemets:");
                for(int i=0;i<N;i++){
                        arr[i]=sc.nextInt();
                }

		int pArr[]=new int[N];
		pArr[0]=arr[0];
		for(int i=1;i<N;i++){
			pArr[i]=pArr[i-1]+arr[i];
		}

                System.out.println("Enter No of Queries:");
                int Q=sc.nextInt();
                //System.out.println("Enter Start & End Index:");
                int sum=0;
		for(int i=1;i<=Q;i++){
                System.out.println("Enter Start & End Index:");
		
			int s=sc.nextInt();
			int e=sc.nextInt();

			if(s!=0){
			sum=pArr[e]-pArr[s-1];
			System.out.println("Sum = "+sum);
			}
			else{
				System.out.println("SUM="+arr[e]);
			}
		}
	}
}
