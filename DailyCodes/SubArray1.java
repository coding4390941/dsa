

/*	Find the shortest subarray conatining both min &max
 *	GIven n Integer of size N Return thr length of the smallest subarray which contains both max of array &min of array 
 *
 *	e.g. A[1,2,3,1,3,4,6,4,6,3]
 *	*/

//   		BRUTE FORCE APPROACH

import java.util.*;

class SubArray{
	public static void main(String []args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int N=sc.nextInt();
		int arr[]=new int[N];

		System.out.println("Enter The Array Elements:");
		for(int i=0;i<N;i++){
			arr[i]=sc.nextInt();
		}
		int max=Integer.MIN_VALUE;
		int min=Integer.MAX_VALUE;

		for(int i=0;i<N;i++){

			if(arr[i]>max){
				max=arr[i];
			}
			if(arr[i]<min){
				min=arr[i];
			}
		}

		int minLen=Integer.MAX_VALUE;
		int len=0;
		for(int i=0;i<N;i++){

			//int len=0;

			if(arr[i]==min){
				for(int j=i+1;j<N;j++){

					if(arr[j]==max){
						len=j-i+1;
						if(minLen>len){
							minLen=len;
						}
					}
				}
			}else{
				//int len=0;
				for(int j=0;j<N;j++){

					if(arr[j]==min){
						len=j-i+1;
						if(minLen>len){
							minLen=len;
						}
					}
				}
			
			}
		}

			System.out.println("Sub Array is: "+minLen);
	}
}
