
// given array, Count 	the no.of elements having atleast 1 element grater than itself


// bruteForce Approach
/*
class GreaterElemnts{

	public static void main(String []args){

		int arr[]=new int[]{2,5,1,4,8,0,8,1,3,8};

		int count=0;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr.length;j++){

				if(arr[i]<arr[j]){
					count++;
					break;
				}
			}
		}
		System.out.println(count);
	}
}

*/

// OPTIMIZED WAY:

class GreaterElemnts{

        public static void main(String []args){

                int arr[]=new int[]{2,5,1,4,8,0,8,1,3,8};

                int count=0;
		int N=10;
		int maxVal=Integer.MIN_VALUE;

                for(int i=0;i<arr.length;i++){
			if(maxVal<arr[i]){
				maxVal=arr[i];
			}
		}
		for(int i=0;i<N;i++){
			if(arr[i]==maxVal){
				count++;
			}
		}


		System.out.println("Count :"+ (N-count));
	}
}












