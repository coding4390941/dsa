/*
 * Q7. Leaders in an array

Problem Description
- Given an integer array A containing N distinct integers, you have to find all
the leaders in array A. An element is a leader if it is strictly greater than all

the elements to its right side.

NOTE: The rightmost element is always a leader.
Problem Constraints
1 <= N <= 105
1 <= A[i] <= 108

Example Input
Input 1:
A = [16, 17, 4, 3, 5, 2]
Input 2:
A = [5, 4]

Example Output
Output 1:
[17, 2, 5]
Output 2:
[5, 4]
*/

import java.util.*;

class LeaderinArray{

	public static void main(String[]args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int N=sc.nextInt();
		int arr[]=new int[N];
		System.out.println("Enter The Elements Of Array:");

		for(int i=0;i<N;i++){
			arr[i]=sc.nextInt();
		}

		
		for(int i=0;i<N;i++){
			boolean flag=true;
		//	int num=arr[i];

			for(int j=i+1;j<N;j++){	
				if(arr[i]<=arr[j]){
					flag=false;
				}
			}
			if(flag){
				System.out.print(" "+arr[i]);
			}

		}

	}
}
