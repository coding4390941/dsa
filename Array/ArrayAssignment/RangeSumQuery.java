/*
  2] Range sum query

 Problem Description
- You are given an integer array A of length N.
- You are also given a 2D integer array B with dimensions M x 2, where

each row

denotes a [L, R] query.
- For each query, you have to find the sum of all elements from L to R

indices

in A (0 - indexed).
- More formally, find A[L] + A[L + 1] + A[L + 2] +... + A[R - 1] + A[R] for each
query.

Problem Constraints
1 <= N, M <= 103
1 <= A[i] <= 105
0 <= L <= R < N

Example Input
Input 1:
A = [1, 2, 3, 4, 5]
B = [[0, 3], [1, 2]]
Input 2:
A = [2, 2, 2]
B = [[0, 0], [1, 2]]

Example Output
Output 1:
[10, 5]
*/
import java.util.*;

class RangeSumQuery{

	static int[] rangeSum(int [],int [][]querie){


	}
	
	public static void main(String []args){

                Scanner sc=new Scanner(System.in);
                System.out.println("Enter Size of array:");
                int N=sc.nextInt();
                int arr[]=new int[N];

                System.out.println("Enter The Array Elements:");

                for(int i=0;i<arr.length;i++){

                        arr[i]=sc.nextInt();
                }

		System.out.println("Enter Queries number (2 Times)");

		int queries[][]=new int[2][2];
                System.out.println("Enter The Query Elements:");
			for(int i=0;i<queries.length;i++){
				for(int j=0;j<queries.length;j++){
					queries[i][j]=sc.nextInt();
				}
			}



 }
