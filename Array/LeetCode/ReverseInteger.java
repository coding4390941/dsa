
 // reverse Integer
 /* 1. Reverse Integer (Leetcode:- 7)

	Given a signed 32-bit integer x, return x with its digits reversed. If reversing
	x causes the value to go outside the signed 32-bit integer range [-231, 231
	- 1], then return 0.
	Assume the environment does not allow you to store 64-bit integers (signed
	or unsigned).
	Example 1:
	Input: x = 123
	Output: 321
	Example 2:
	Input: x = -123
	Output: -321
	Example 3:
	Input: x = 120
	Output: 21

	Constraints:
			-231 <= x <= 231 - 1

*/

import java.util.*;
class Reverse{

	static long reverse(long num){

		long rnum=num;
		boolean flag=true;
		if(num < 0){
			flag=false;
			rnum = -num;
		}
		long num2=0;

		while(rnum!=0){
			
			long rem = rnum%10;

			if (num2 > Integer.MAX_VALUE / 10 || (num2 == Integer.MAX_VALUE / 10 && rem > 7)) {
         			   return 0;
        		}
      			  if (num2 < Integer.MIN_VALUE / 10 || (num2 == Integer.MIN_VALUE / 10 && rem < -8)) {
            			return 0;
        		}
			rnum = rnum/10;
			num2=rem+num2*10;
		}

		if(num2 > Math.pow(2,30) || num2 < Math.pow(2,-31)){
			return 0;
		}else{
			if(flag){
			return num2;
			}else{
				return -num2;
			}
		}
	}

	public static void main(String []args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number:");
		long num=sc.nextLong();
		long rev=reverse(num);

		System.out.println("Reverse NO Is: "+rev);
	

	}
}
