/*
 * 2. Two Sum (Leetcode:- 1)

Given an array of integer numbers and an integer target, return indices of
the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you
may not use the same element twice.
You can return the answer in any order.

Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
*/

import java.util.*;
class TwoSum{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the Array Elements:");
		for(int i=0;i<size;i++){

			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter Integer Target:");
		int z=sc.nextInt();
		int retInd[]=new int[2];
		int itr=0;

		for(int i=0;i<size-1;i++){
			//itr++;
			if(arr[i]+arr[i+1]==z){
				retInd[0]=i;
				retInd[1]=i+1;
				break;
			}
		}
		
		for(int data:retInd){

		System.out.print(data+ " ");
		}

	//	System.out.println(itr);


	}
}
