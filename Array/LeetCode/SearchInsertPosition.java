
/* 3. Search Insert Position (LeetCode-35)

Given a sorted array of distinct integers and a target value, return the index
if the target is found. If not, return the index where it would be if it were
inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4

Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
*/

// BruteForce Approach


import java.util.*;
class SearchInsertPosition{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int N=sc.nextInt();

		int arr[]=new int[N];
		System.out.println("Enter Array Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}

		System.out.println("Enter Target Value:");
		int num=sc.nextInt();

		int res=searchPosition(arr,num);
		
		System.out.println("Output is: "+res);


	}

		static int searchPosition(int []arr,int num){
			
			int temp=0;
			for(int i=0;i<arr.length-1;i++){
				if(arr[i]==num){
			
					return i;
				}
			       	if((arr[i] < num)&&(arr[i+1]>num)){
						return i+1;
				}
				if(i==arr.length-1){
					
					if(arr[i]>num){
						temp=i+1;
						return temp;
					}
				}

			}
			

			return 110;
		}
}
